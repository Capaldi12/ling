mongoengine~=0.23.0
requests~=2.18.4
beautifulsoup4~=4.9.3
tqdm~=4.60.0

Flask~=2.0.0
pytz~=2021.1

pyspark~=3.0.0.dev2
pymorphy2~=0.9.1
natasha~=1.4.0
selenium~=3.141.0

nltk~=3.6.1
pymystem3~=0.2.0
