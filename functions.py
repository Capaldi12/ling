"""Вспомогательные функции"""
__all__ = ['parse_date', 'get_unique_str_ind', 'read_file', 'rewrite_file']

from datetime import datetime


def parse_date(date_str: str) -> datetime:
    """
    Парсит строку даты из html в datetime

    :param date_str: строка даты
    :return: datetime представление
    """

    # Удалить ':' потому что питон не умеет его парсить
    if ":" == date_str[-3:-2]:
        date_str = date_str[:-3] + date_str[-2:]

    return datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S%z")


def get_unique_str_ind(first_part, letter_1, letter_2):
    """
    Возвращает уникальный строковый индекс (не являющийся частью  другого строкового индекса)

    :param first_part: первая часть
    :param letter: заглавная буква
    :param digit: цифра
    :return: уникальный строковый индекс, следующая буква, следующая цифра
    """
    index=first_part+letter_1+letter_2
    if chr(ord(letter_2)+1)=='K':
        new_letter_1=chr(ord(letter_1)+1)
        new_letter_2='A'
    else:
        new_letter_1=letter_1
        new_letter_2=chr(ord(letter_2)+1)
    return index, new_letter_1, new_letter_2


def read_file(path_to_file):
    file = open(path_to_file, "r")
    file_content = file.read()
    file.close()
    return file_content


def rewrite_file(path_to_file, content):
    file = open(path_to_file, "w")
    file.write(content)
    file.close()
