import sys
sys.path.insert(0, '..')     #Чтобы найти модуль constants, необходимо добавить путь к нему, поскольку модуль не содержится в данной директории

from mongoengine import *
from bs4 import BeautifulSoup
from selenium import webdriver
import time
from constants import LANDMARKS_URL, CHROMDRIVER_PATH
from mongo import connection, Landmark
from functions import get_unique_str_ind


def bs4_soup_to_str(bs4_soup):
    lst=[str(item) for item in bs4_soup]
    return ' '.join(lst)

def get_text_in_tags(bs4_soup):
    lst=[]
    for item in bs4_soup:
        string=str(item)
        start=string.find('>')+1
        end=string.rfind('<')
        landmark=string[start:end]
        string_with_fixed_spaces=''
        items=landmark.split('.')
        for item in items:
            if string_with_fixed_spaces!='':
                string_with_fixed_spaces=string_with_fixed_spaces+'.'
                if not item.startswith(' '):
                    string_with_fixed_spaces=string_with_fixed_spaces+' '
            string_with_fixed_spaces=string_with_fixed_spaces+item                                            
        lst.append(string_with_fixed_spaces)
    return lst

def get_landmarks():
    url=LANDMARKS_URL
    chromedriver = CHROMDRIVER_PATH
    options = webdriver.ChromeOptions()
    options.add_argument('headless')                                           # для открытия headless-браузера (без GUI)
    browser = webdriver.Chrome(executable_path=chromedriver, chrome_options=options)
    browser.get(url)
    get_more_button=browser.find_element_by_id("true-loadmore")
    get_more_button.click()
    time.sleep(5)                                                              #Без этого страница не всегда успевает динамически добавить элементы по нажатию
    html_text=browser.execute_script("return document.documentElement.outerHTML;")
    soup = BeautifulSoup(html_text, 'lxml')
    soup_tags=soup.find_all("div", {"class": "ta-211"})
    soup_0=BeautifulSoup(bs4_soup_to_str(soup_tags), 'lxml')
    soup_a_tags=soup_0.find_all('a')
    landmarks=get_text_in_tags(soup_a_tags)
    return landmarks

def fill_landmarks_collection():
    print("========================================Landmarks_parsing========================================")
    name_lst=get_landmarks()
    with connection():
        Landmark.drop_collection()                                       
        letter_1='A'
        letter_2='A'
        for item in name_lst:
            an_index, letter_1, letter_2 = get_unique_str_ind('LandmarkIndex', letter_1, letter_2)
            Landmark(name=item, index=an_index).save()
            print("Parsed: "+item)

if __name__ == '__main__':
    fill_landmarks_collection()
    #Можно посмотреть, что заполнилось раскомментировав
    """with connection():
        for landmark in Landmark.objects:
            print(landmark.name+' '+landmark.index)"""

#Загрузить драйверы можно на https://www.selenium.dev/documentation/en/webdriver/driver_requirements/
#$wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
#$wget http://bbgentoo.ilb.ru/distfiles/google-chrome-stable_48.0.2564.116-1_i386.de
#$google-chrome --version (У меня Google Chrome 90.0.4430.212, соответственно драйвер под нее)
#Поместить драйвер в папку Drivers
#$sudo nano /etc/environment
#Добавить к путям папку с драйверами (у меня /home/vagrant/KW/ling/Drivers)
