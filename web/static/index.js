// Эта функция отправляет запрос по адресу /parse, который
// инициализирует парсинг новостей с первой страницы
function updateDatabase() {
    // Прилепляем уведомление о загрузке
    $("#upd-results").html(`Fetching...`)
    let q = $('#parse-input').val()

    // Посылаем запрос при помощи jquery
    $.getJSON(`/parse?q=${q}`, function (data) {
        // Это коллбэк на результат запроса

        // Находим тэг где кнопка
        const results = $("#upd-results")

        // Если успешно
        if (data.code === 200) {
            // Пишем что успех
            results.html(`Success! ${data.count} articles added`)

            // Предлагаем перезагрузить страницу, если есть смысл
            if (data.count > 0 &&
                confirm(`${data.count} articles was loaded or updated. Refresh page?`)) {

                location.assign('/index')
            } else {
                // Или говорим, что ничего не загрузилось
                results.html(`No articles was found or updated`)
            }

        } else {
            results.html(`Error: ${data.code} ${data.name}`)
        }

    }).fail(function (data) {
        //Коллбэк на ошибку
        $("#upd-results").html(`Error: ${data.status} ${data.statusText}`)
    })

}


function updateW2V() {
    $("#upd-results").html(`Fetching...`)

    $.getJSON('/w2v', function (data) {
        const results = $("#upd-results")

        if (data.code === 200) {

            results.html('Success! Synonyms updated')

            if (confirm('Synonyms was updated. Refresh page?')) {

                location.assign('/index')
            }

        } else {

            results.html(`Error: ${data.code} ${data.name}`)

        }

    }).fail(function (data) {
        $("#upd-results").html(`Error: ${data.status} ${data.statusText}`)
    })

}

var previous = 1

function validate(input_field) {
    const pattern = /^((\w+)-?(\w+)?)?$/

    if (pattern.test(input_field.value))
        previous = input_field.value
    else
        input_field.value = previous
}
