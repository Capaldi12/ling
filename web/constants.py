"""Константы связанные с веб интерфейсом"""

PER_PAGE = 40
DEFAULT_TIMEZONE = 'Europe/Moscow'
DATE_FORMAT = '%d.%m.%Y %H:%M'
WEBSITE_NAME = 'PV1'
WEBSITE_DESC = 'News parsed'
