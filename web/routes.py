"""Правила возвращения страниц"""

from . import app
from .constants import *

from flask import render_template, request, jsonify

from mongoengine import ValidationError, DoesNotExist
from mongo import connection, Article, Person, Landmark, Sentence

import pipeline
from part2 import w2v

import math
from operator import attrgetter

from traceback import print_exc


@app.route('/')
@app.route('/index')
def index():
    """
    Главная страница. Содержит список статей с общей информацией,
    ссылкой на статью на v1.ru и ссылкой на страницу статьи интерфейса
    """
    back = request.referrer or '/'

    page = request.args.get('page', 0, type=int)

    sort = request.args.get('sort', None)
    order = request.args.get('order', 0, type=int)

    if sort in ['title', 'date', 'views', 'comments', 'update_date']:
        o = '-' if order else ''
    else:
        sort = 'date'
        o = '-'

    with connection():
        count = Article.objects.count()

        # Считаем количество страниц
        page_count = math.ceil(count / PER_PAGE)

        # Достаём статьи из БД, отсортировав в порядке убывания даты
        # а затем выбираем нужные для текущей страницы
        article_list = Article.objects.order_by(o + sort)\
            [page * PER_PAGE : (page+1) * PER_PAGE]

        # Рендерим шаблон с указанием параметров
        return render_template(
            'index.html',
            count=count,   # Общее число статей
            articles=article_list,  # Список статей

            # Параметры навигации
            navparams={
                'page': page,  # Текущая страница
                'count': page_count,  # Общее число страниц
                'previous': page > 0,  # Есть ли предыдущая
                'next': page < page_count - 1  # Есть ли следующая
            },

            back=back
        )



@app.route('/articles/<article_id>')
def articles(article_id):
    """
    Страница статьи. Показывает название, дату публикации,
    количество просмотров и комментариев и текст статьи

    :param article_id: ID статьи в базе данных
    """
    back = request.referrer or '/'

    with connection():
        # Извлекаем статью по id
        try:
            article = Article.objects(id=article_id).get()

            sentences = Sentence.objects(src_to_article=article)

        except (ValidationError, DoesNotExist):

            article = None
            sentences = []

        # Рендерим шаблон
        return render_template(
            'articles.html',
            article=article,
            sentences=sentences,
            back=back)

@app.route('/entities')
@app.route('/entities/<entity_id>')
def entities(entity_id=None):
    """
    Страница сущности или списка сущностей

    Если есть сущность, отображает её данные и список предложений с упоминаниями
    Если сущность отсутствует, отображает список сущностей в БД

    :param entity_id: ID запрашиваемой сущности или None
    """
    back = request.referrer or '/'

    page = request.args.get('page', 0, type=int)

    # Параметры сортировки и фильтрации
    sort = request.args.get('sort', None)
    order = request.args.get('order', 0, type=int)
    only = request.args.get('only', None)

    if sort in ['type', 'name', 'index', 'rating']:
        o = '-' if order else ''
    else:
        sort = 'type'
        o = '-'

    with connection():

        # Сущность есть - отображаем её поля
        if entity_id:
            # Поиск сущности сначала в таблице Person, затем в Landmark
            try:
                entity = Person.objects(id=entity_id).get()
            except (ValidationError, DoesNotExist):
                try:
                    entity = Landmark.objects(id=entity_id).get()
                except (ValidationError, DoesNotExist):
                    entity = None

            sentences = []

            # Предложения, если сущность найдена
            if entity:
                sentences = Sentence.objects(entity_id=str(entity.id))

            return render_template(
                "entities.html",
                is_entity=True,  # Просмотр конкретной сущности
                entity=entity,   # Просматриваемая сущность
                sentences=sentences,  # Предложения с упоминаниями
                back=back
            )

        # Сущности нет - отображаем список
        else:
            entity_list = list(Person.objects) + list(Landmark.objects)

            # Оставляем только один тип сущностей
            if only and only in ('Person', 'Landmark'):
                entity_list = [entity for entity in entity_list if entity.type == only]

            # Сортируем по выбранному полю
            entity_list.sort(key=attrgetter(sort), reverse=o=='-')

            # Переносим сущности без рейтинга в конец списка
            if sort == 'rating':
                rated = [entity for entity in entity_list if entity.rating > -10]
                not_rated = [entity for entity in entity_list if entity.rating == -10]

                entity_list = rated + not_rated

            # Выбираем список для текущей страницы
            page_count = math.ceil(len(entity_list) / PER_PAGE)
            entity_list = entity_list[page * PER_PAGE : (page+1) * PER_PAGE]

            return render_template(
                "entities.html",
                is_entity=False,    # Просмотр всех сущностей
                count=len(entity_list),
                entity_list=entity_list, # Список сущностей

                navparams={
                    'page': page,  # Текущая страница
                    'count': page_count,  # Общее число страниц
                    'previous': page > 0,  # Есть ли предыдущая
                    'next': page < page_count - 1  # Есть ли следующая
                },

                back=back
            )


@app.route('/parse')
def process_first_page():
    """
    Запрос парсинга новостей.
    Можно запустить прямо с сайта при помощи jquery например
    """

    q = request.args.get('q', '1', str).split('-')
    print(q)

    pagelist = []

    if len(q) == 1:
        page = int(q[0])

    else:
        a, b = sorted([int(v) for v in q])
        pagelist = range(b, a-1, -1)

        page = None

    with connection():
        # Запускаем пайплайн
        try:
            if page:
                pipeline.run(page)

            else:
                for page in pagelist:
                    pipeline.run(page)

        except Exception as e:
            print_exc()

            return jsonify({
                'code': 500, 'name': 'Internal server error',
                'message': 'Error while parsing', 'exc': str(e)
            })

        count = len(pipeline.get_intermediate(0))

        # Возвращаем сигнал успеха и количество распарсеных новостей
        return jsonify({'code': 200, 'name': 'OK', 'count': count})


@app.route('/w2v')
def get_w2v():
    """Запуск заполнения синонимов"""

    with connection():
        try:
            w2v()

        except Exception as e:
            print_exc()

            return jsonify({
                'code': 500, 'name': 'Internal server error',
                'message': 'Error while finding synonyms', 'exc': str(e)
            })


        return jsonify({'code': 200, 'name': 'OK'})

@app.errorhandler(404)
def not_found(e):
    return render_template('error.html', error=e)
