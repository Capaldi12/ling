"""Маппинг документов"""

from mongoengine import Document, \
    StringField, IntField, FloatField, DateTimeField, \
    ListField, ReferenceField, \
    DoesNotExist

from datetime import datetime
import pytz

from typing import Optional, Union

from .constants import DEFAULT_DATABASE, DEFAULT_TIMEZONE, DATE_FORMAT


class Article(Document):
    """Новостная статья"""

    meta = {"db_alias": DEFAULT_DATABASE}

    link = StringField(required=True, unique=True)  # Ссылка на новость

    title = StringField(required=True)  # Название новости
    content = ListField(StringField())  # Текст новости, разделённый по абзацам

    date = DateTimeField(required=True)   # Дата новости

    views = IntField(min_value=0, default=0)   # Количество просмотров новости (если есть)
    comments = IntField(min_value=0, default=0)    # Количество комментариев новости (если есть)

    update_date = DateTimeField()   # Дата последнего изменения записи в БД

    def clean(self):
        # Обновляет дату обновления записи
        self.update_date = datetime.utcnow()

    def __repr__(self):
        return f'Article({self.short_title} | {self.link})'

    __str__ = __repr__

    @property
    def url(self) -> str:
        """Возвращает url страницы статьи в интерфейсе"""

        return f'/articles/{self.id}'

    @property
    def short_title(self) -> str:
        """Сокращённая версия названия (до 50 символов)"""

        if len(self.title) <= 50:
            return self.title
        else:
            return self.title[:47].strip() + '...'

    @property
    def text(self) -> str:
        """Полный текст новости одной строкой"""
        if len(self.content):
            return '\n'.join(self.content)
        else:
            return 'No text'

    # Правильное форматирование дат
    @property
    def local_date(self) -> datetime:
        """Дата в местном часовом поясе"""

        return self.date.astimezone(pytz.timezone(DEFAULT_TIMEZONE))

    @property
    def local_date_str(self) -> str:
        """Дата в местном часовом поясе строкой вида YYYY-MM-DD hh:mm"""

        return self.local_date.strftime(DATE_FORMAT)

    @property
    def local_update_date(self) -> datetime:
        """Дата обновления в местном часовом поясе"""

        return self.update_date.astimezone(pytz.timezone(DEFAULT_TIMEZONE))

    @property
    def local_update_date_str(self) -> str:
        """Дата обновления в местном часовом поясе строкой вида YYYY-MM-DD hh:mm"""

        return self.local_update_date.strftime(DATE_FORMAT)


class Person(Document):
    """Известная личность"""

    meta = {"db_alias": DEFAULT_DATABASE}

    name = StringField(required=True, max_length=64)
    index = StringField(required=True, max_length=64)

    synonyms = ListField(StringField())
    environment_words = ListField(StringField())

    rating = FloatField()

    def __repr__(self):
        return f'Person({self.name})'

    __str__ = __repr__

    @property
    def url(self) -> str:
        return f'/entities/{self.id}'

    @property
    def type(self) -> str:
        return 'Person'

    @property
    def rating_round(self, r=2):
        return round(self.rating, r)


class Landmark(Document):
    """Географический объект"""

    meta = {"db_alias": DEFAULT_DATABASE}

    name = StringField(required=True, max_length=128)
    index = StringField(required=True, max_length=64)

    synonyms = ListField(StringField())
    environment_words = ListField(StringField())

    rating = FloatField()

    def __repr__(self):
        return f'Landmark({self.name})'

    __str__ = __repr__

    @property
    def url(self) -> str:
        return f'/entities/{self.id}'

    @property
    def type(self) -> str:
        return 'Landmark'

    @property
    def rating_round(self, r=2):
        return round(self.rating, r)


class Sentence(Document):
    """Предложение, содержащее именованную сущность"""

    meta = {"db_alias": DEFAULT_DATABASE}

    src_to_article = ReferenceField(Article, required=True)

    content = StringField(required=True)

    entity_id =  StringField(required=True)
    entity_type = StringField(required=True)

    tonality = FloatField(required=True, default=0)

    _tonality_map = {
        -2: 'Negative',
        -1: 'MostlyNegative',
        0: 'Neutral',
        1: 'MostlyPositive',
        2: 'Positive'
    }

    @property
    def tonality_class(self):
        return self._tonality_map.get(self.tonality, 'Error')

    @property
    def entity(self) -> Optional[Union[Person, Landmark]]:
        try:
            return Person.objects(id=self.entity_id).get()

        except DoesNotExist:
            try:
                return Landmark.objects(id=self.entity_id).get()

            except DoesNotExist:
                return None

    @property
    def url(self) -> str:
        entity = self.entity

        if entity is not None:
            return entity.url

        return '/entities/Error'


    def __repr__(self):
        content = self.content[:47].strip() + "..." \
            if len(self.content) > 50 else self.content

        return f'Sentence({content})'

    __str__ = __repr__
