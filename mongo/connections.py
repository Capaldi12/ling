"""Контроль подключений"""

from mongoengine import connect as _connect, disconnect as _disconnect

from functools import partial

from .constants import DEFAULT_DATABASE


# Частичные вызовы с уже переданным названием БД в качестве параметра
connect = partial(_connect, DEFAULT_DATABASE, DEFAULT_DATABASE)
disconnect = partial(_disconnect, DEFAULT_DATABASE)


class connection(object):
    """Контекст-менеджер для подключения к БД"""

    connections = {}  # Количество открытых соединений

    def __init__(self, name=DEFAULT_DATABASE):
        self.name = name

    def __enter__(self):
        # Добавляем запись в словарь, если отсутствует и получаем её
        c = self.connections.setdefault(self.name, 0)

        if c == 0:  # Нет активных соединений, открываем новое
            _connect(self.name, self.name)

        self.connections[self.name] += 1  # Учитываем

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Понижаем количество соединений
        self.connections[self.name] -= 1

        # Если активных соединений не осталось, значит можно закрывать
        if self.connections[self.name] == 0:
            _disconnect(self.name)
