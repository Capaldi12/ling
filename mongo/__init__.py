"""
Интерфейс к базе данных монго

- connect, disconnect - открыть/закрыть подключение к БД. Для интерактивного режима
- connection - менеджер контекста для работы с БД. Сам управляет подключением

- Article - новостная статья
- Sentence - предложение из статьи с именованной сущностью
- Person - Известная личность
- Landmark - Географический объект
"""

from .connections import connect, disconnect, connection
from .documents import Article, Sentence, Person, Landmark
from .constants import DEFAULT_DATABASE
