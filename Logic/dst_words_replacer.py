from genericpath import exists
import sys
sys.path.insert(0, '..')
sys.path.insert(0, '../Parsers')

from constants import ARTICLES_PATH
import os
import re
from mongo import connection, Article, Sentence, Person, Landmark, DEFAULT_DATABASE
from mongoengine import *
from constants import EXCHANGER_PATH
from functions import rewrite_file, read_file
from word2vec import get_new_model, find_synonyms_for_words
from articles_preparation import split_text_to_sentences, remove_excess_for_word2vec, normalize_text, generate_article_files
import pymorphy2
from natasha import Doc, Segmenter, NewsEmbedding, NewsSyntaxParser
from nltk.corpus import stopwords
from articles_preparation import normalize_text, remove_excess_for_word2vec


def get_normalized_persons_for_replace(person_fio_lst, morph):
    """
    Лемматизатор может каврекать некоторые имена и фамилии, но посколько статьи будут лемматизироватся,
    они всеравно будут находится в тексте. Это позволить находить персон в разных формах (при указании в тексте в формате: фамилия имя)
    """
    normalized_persons=[]
    for fio in person_fio_lst:
        fio_items=fio.split(' ')
        lemmatized_fio_items=[morph.parse(item)[0].normal_form for item in fio_items] #Выбираем для всех слов первую нормальную форму ([0])
        rep_text=lemmatized_fio_items[0]+' '+lemmatized_fio_items[1]
        if len(fio_items)==3:
            rep_text=rep_text+' '+lemmatized_fio_items[2]
        normalized_persons.append(rep_text)
    return normalized_persons

def get_normalized_landmarks_for_replace(landmark_name_lst, morph):
    normalized_strings_lst=[]
    for landmark_name in landmark_name_lst:
        string=remove_excess_for_word2vec(landmark_name)
        string=normalize_text(string, morph)
        normalized_strings_lst.append(string)
    return normalized_strings_lst

def get_person_combinations(fio):
    combinations=[]
    fio_item=fio.split(' ')
    if(len(fio_item)==3):
        combinations.append(fio_item[0]+' '+fio_item[1]+' '+fio_item[2])
        combinations.append(fio_item[1]+' '+fio_item[2]+' '+fio_item[0])
        combinations.append(fio_item[1]+' '+fio_item[2])
    combinations.append(fio_item[1]+' '+fio_item[0])
    combinations.append(fio_item[0]+' '+fio_item[1])
    #Если брать только фамилию, может набится много однофамильцев
    #Некоторые фамилии нормализуются в обычные слова
    #if fio_item[0]!="долг":
    #    combinations.append(fio_item[0])
    return combinations

def get_landmark_combinations(landmark):
    #Где-то тут должна быть картинка с Джеком Воробьем (грустно и топорно, свсем не помидорно)... Но пока так)
    #Регулярки захватывают части слов, которые не относятся к тому, что нужно, а так происходит сравнение слов без лишних входов
    #Дольше писать, но сложнее сломать при внесении правок после обнаружения чего-либо невходящего
    #Больше - не всегда лучше, в том числе и в машинном обучении, пытаемся учесть в первую очередь наиболее вероятное
    landmarks=\
        {
            "казанский кафедральный собор": ["казанский кафедральный собор", "кафедральный собор", " собор кафедральный", " собор казанский"],
            "волгоградский областной филармония": ["волгоградский областной филармония", "волгоградский филармония", " филармония волгоградский", "областной филармония", "филармония областной"],
            "авангард": ["авангард"],
            "областной универсальный научный библиотека им. м. горький": ["областной универсальный научный библиотека им. м. горький", "универсальный научный библиотека они м горький", "научный библиотека они м горький",\
            "научный библиотека они м горький",  "библиотека они м горький", "библиотека они горький", "библиотека горький", "горький библиотека"],
            "площадь пасть борец": ["площадь пасть борец"],
            "памятник саша филиппов": ["памятник саша филиппов", "памятник филиппов саша", " филиппов памятник", "памятник филиппов", "саша филиппов"],
            "музей история кировский район": ["музей история кировский район", "музей кировский район"],
            "памятник чекист": ["памятник чекист"],
            "армянский церковь святой георгий": ["армянский церковь святой георгий", "армянский церковь георгий", "церковь георгий армянский", "церковь святой георгий", "церковь георгий",\
            "святой георгий церковь", "георгий церковь"],
            "трамвай-памятник": ["трамвай-памятник"],
            "воинский эшелон": ["воинский эшелон", "эшелон воинский", "эшелон"],
            "мельница гергардт": ["мельница гергардт", "гергардт мельница"],
            "памятник дзержинский": ["памятник дзержинский", "дзержинский памятник"],
            "здание царицынский пожарный команда": ["здание царицынский пожарный команда", "царицынский пожарный команда здание", "царицынский пожарный команда", "пожарный команда царицынский"],
            "дом павлов": ["дом павлов", "павлов дом"],
            "челябинский колхозник": ["челябинский колхозник", "колхозник челябинский"],
            "памятник гоголь": ["памятник гоголь", "гоголь памятник"],
            "бк-13": ["бк-13"],
            "бейт давид": ["бейт давид", "давид бейт"],
            "бармалей": ["бармалей"],
            "лысый гора": ["лысый гора", "гора лысый"],
            "элеватор": ["элеватор"],
            "памятник паникаха": ["памятник паникаха", "паникаха памятник"],
            "фонтан влюбить": ["фонтан влюбить", "влюбить фонтан"],
            "музей-квартира луконин м. к. ": ["музей-квартира луконин м к", "музей-квартира луконин", "луконин м к музей-квартира", "луконин музей-квартира"],
            "музейно-выставочный центр красноармейский район": ["музейно-выставочный центр красноармейский район", "музейно-выставочный центр"],
            "мамай курган": ["мамай курган"],
            "речной порт": ["речной порт"],
            "царицынский опера": ["царицынский опера"],
            "музей история здравоохранение": ["музей история здравоохранение", "музей здравоохранение", "здравоохранение музей"],
            "музей ребёнок царицын сталинград волгоград": ["музей ребёнок царицын сталинград волгоград", "ребёнок царицын сталинград волгоград"],
            "народный музей волгоградский железнодорожник": ["народный музей волгоградский железнодорожник", "музей волгоградский железнодорожник", "волгоградский железнодорожник народный музей", "волгоградский железнодорожник музей"],
            "музей авиация": ["музей авиация", "авиация музей"],
            "выставочный зал волгоградский музей изобразительный искусство им. и. и. машков": ["выставочный зал волгоградский музей изобразительный искусство им. и. и. машков", "зал волгоградский музей изобразительный искусство им. и. и. машков",\
            "зал музей изобразительный искусство им. и. и. машков", "зал музей изобразительный им. и. и. машков", "зал музей изобразительный искусство машков"],
            "военно-патриотический музей история связь и радиолюбительство царицына-сталинграда-волгоград": ["военно-патриотический музей история связь и радиолюбительство царицына-сталинграда-волгоград", "музей история связь радиолюбительство царицына-сталинграда-волгоград",\
            "музей связь радиолюбительство царицына-сталинграда-волгоград", "музей радиолюбительство царицына-сталинграда-волгоград", "музей история связь", "музей связь"],
            "парк имя юрий гагарин": ["парк имя юрий гагарин", "имя юрий гагарин парк", "парк юрий гагарин", "юрий гагарин парк", "парк гагарин", "гагарин парк"],
            "музей история волго-донской судоходный канал": ["музей история волго-донской судоходный канала", "музей история волго-донской канала", "музей волго-донской судоходный канала", "музей волго-донской канала",\
            "история волго-донской судоходный канала музей", "история волго-донской канала музей",   "волго-донской судоходный канала музей", "волго-донской канала музей"],
            "выставочный зал волгоградский областной организация союз художник россия": ["выставочный зал волгоградский областной организация союз художник россия", "зал волгоградский областной организация союз художник россия", "зал волгоградский областной организация союз художник россия",
            "зал областной организация союз художник россия", "зал организация союз художник россия", " союз художник россия зал организация", "зал союз художник россия", "союз художник россия зал"],
            "парк победа": ["парк победа", "победа парк"],
            "комсомольский сад": ["комсомольский сад", "сад комсомольский"],
            "детский городской парк": ["детский городской парк", "детский парк городской", "городской парк детский", "городской детский парк", "парк городской детский", "парк детский городской"],
            "сквер имя саша филиппов": ["сквер имя саша филиппов", "имя саша филиппов сквер", "сквер саша филиппов", "саша филиппов сквер", "сквер имя филиппов", "имя филиппов сквер", "филиппов сквер", "сквер филиппов"],
            "парк дружба волгоград-баку": ["парк дружба волгоград-баку", "волгоград-баку парк дружба", "парк волгоград-баку", "волгоград-баку парк"],
            "зал воинский слава": ["зал воинский слава", "воинский слава зал"],
            "ботанический сад вгсп": ["ботанический сад вгсп", "сад вгсп", "вгсп сад"],
            "цпкио": ["цпкио"],
            "музей музыкальный инструмент им. е. н. пушкин": ["музей музыкальный инструмент им. е. н. пушкин", "они быть н пушкин музей музыкальный инструмент", "музей музыкальный инструмент они н пушкин",\
            "быть н пушкин музей музыкальный инструмент", "музей музыкальный инструмент пушкин", "пушкин музей музыкальный инструмент", "пушкин музей инструмент", "музей инструмент пушкин"],
            "музей мера и вес": ["музей мера вес", "мера вес музей"],
            "областной краеведческий музей": ["областной краеведческий музей", "краеведческий музей областной", "краеведческий музей", "музей краеведческий"],
            "музей-заповедник старый сарепта": ["музей-заповедник старый сарепта", "старый сарепта музей-заповедник", "старый сарепта"],
            "планетарий": ["планетарий"],
            "память": ["память"],
            "волгоградский музей изобразительный искусство им. и. и. машков": ["волгоградский музей изобразительный искусство им. и. и. машков", "музей изобразительный искусство они и и машков", "музей искусство они и и машков",\
            "музей им. и. и. машков", "они и и машков музей",  "музей и и машков", "и и машков музей", "музей и машков", "и машков музей", "музей машков", "машков музей"],
            "музей-панорама сталинградский битва": ["музей-панорама сталинградский битва", "сталинградский битва музей-панорама"],
            "мемориально-исторический музей": ["мемориально-исторический музей", "музей мемориально-исторический"],
            "горсад": ["горсад"],
            "волгоград арена": ["волгоград арена", "арена волгоград"]
        }
    return landmarks[landmark]

def get_person_indexes():
    return [person.index for person in Person.objects] 

def get_landmark_indexes():
    return [landmark.index for landmark in Landmark.objects]

def replace_substr(substring, new_substr, sent):
    #string="кот кот-муха кот-жмот кот покотао кота локот абормот-кот кот-кот кот кот кот коток кот коток"
    replaced_sent=re.sub(r"(^"+substring+r"$)|(^"+substring+r"\b)|(\b"+substring+r"$)", new_substr, sent)
    replaced_sent=re.sub(r"\s"+substring+r"\s", " "+new_substr+" ", replaced_sent) # ' кот кот кот'->'_cat_кот кот'->'_cat_кот_cat_'  _ - использованные пробелы (вторично при замене не используются) 
    replaced_sent=re.sub(r"\s"+substring+r"\s", " "+new_substr+" ", replaced_sent) # ' cat кот cat '->' cat_cat_cat '. Поэтому два раза
    return replaced_sent

def replace_person(id_article, article, person_fio, person_index, sents_with_person):
    combinations=get_person_combinations(person_fio)
    sents_lst=article.split('\n')
    replaced_sents_lst=[]
    for i_sent, sent in enumerate(sents_lst):
        replaced_sent=sent
        for comb in combinations:
            previous_replaced_sent = replaced_sent
            replaced_sent = replace_substr(comb, person_index, previous_replaced_sent)
            if previous_replaced_sent!=replaced_sent:
                sents_with_person.append([id_article, i_sent, person_index])                  
        replaced_sents_lst.append(replaced_sent)
    return ['\n'.join(replaced_sents_lst), sents_with_person]

def replace_landmark(id_article, article, landmark_name, landmark_index, sents_with_landmark):
    combinations=get_landmark_combinations(landmark_name)
    sents_lst=article.split('\n')
    replaced_sents_lst=[]
    for i_sent, sent in enumerate(sents_lst):
        replaced_sent=sent
        for comb in combinations:
            previous_replaced_sent = replaced_sent
            replaced_sent = replace_substr(comb, landmark_index, previous_replaced_sent)
            if previous_replaced_sent!=replaced_sent:
                sents_with_landmark.append([id_article, i_sent, landmark_index])
        replaced_sents_lst.append(replaced_sent)
    return ['\n'.join(replaced_sents_lst), sents_with_landmark]

def get_sents_with_dst_entity(db_class, sents_with_ent_lst):
    sents_with_ent=[]
    with connection():  
        for sent in sents_with_ent_lst:
            article_id=sent[0]
            i_sent=sent[1]
            text=str(Article.objects.get(id=article_id).content)
            sents_lst=split_text_to_sentences(text).split('\n')
            sentence=sents_lst[i_sent]
            entity_ind=sent[2]
            entity_id=db_class.objects(index=entity_ind).first().id
            sents_with_ent.append([article_id, sentence, entity_id])
    return sents_with_ent

def fill_Sentences_collection(sents_with_person, sents_with_landmark):
    person_sents_to_write=get_sents_with_dst_entity(Person, sents_with_person)
    landmark_sents_to_write=get_sents_with_dst_entity(Landmark, sents_with_landmark)
    sents_id_lst=[]
    with connection():
        #Sentence.drop_collection()                                
        for person_sent in person_sents_to_write:
            article_id, sentence, ent_ind = person_sent
            try:
                tmp=Sentence.objects.get(content=sentence, entity_type="person")
            except Exception as e:
                #print(e)
                print("=========================================================================================")
                print(sentence, "Такого предложения с персоной пока нет в базе данных, заносим его туда", sep='\n')                     
                Sentence(src_to_article=article_id, content=sentence, entity_id=str(ent_ind), entity_type="person").save()
                sent_id=Sentence.objects.order_by('-id').first().id
                sents_id_lst.append(sent_id)
        for landmark_sent in landmark_sents_to_write:
            article_id, sentence, ent_ind = landmark_sent
            try:
                tmp=Sentence.objects.get(content=sentence, entity_type="landmark")==None
            except Exception as e:
                #print(e)
                landmark_name=Landmark.objects.get(id=ent_ind).name
                if landmark_name!="Память" or landmark_name=="Память" and  re.search(r"Памят", sentence):
                    print("=========================================================================================")
                    print(sentence, "Такого предложения с достопримечательностью пока нет в базе данных, заносим его туда", sep='\n')                                     
                    Sentence(src_to_article=article_id, content=sentence, entity_id=str(ent_ind), entity_type="landmark").save()
                    sent_id=Sentence.objects.order_by('-id').first().id
                    sents_id_lst.append(sent_id)

    return sents_id_lst

def get_sentence_and_index(sent_with_ent):
    article_id = sent_with_ent[0]
    sentence_number = sent_with_ent[1]
    index = sent_with_ent[2]
    an_article_path=ARTICLES_PATH+"/article_"+article_id+".txt"
    article = read_file(an_article_path)
    sentns_lst=article.split('\n')
    text=sentns_lst[sentence_number]
    return text, index

def get_token_with_text(doc, text):
    a_token=None
    for token in doc.tokens:
        if token.text==text:
            a_token = token
            break
    return a_token

def get_lower_words(doc, env_words, a_token):
    #Ниже по синтаксическому дереву (ближе к листьям)
    for token in doc.tokens:
        if token.head_id==a_token.id:
            env_words.append(token.text)

def get_upper_words(doc, env_words, a_token):
    #Выше по синтаксическому дереву (ближе к корню)
    for token in doc.tokens:
        if token.id==a_token.head_id:
            env_words.append(token.text)

def append_unique_words_to_dict(dic, index_key, env_words):
    env_word_lst = dic[index_key]
    for word in env_words:
        if word not in env_word_lst:
            env_word_lst.append(word)
    dic[index_key] = env_word_lst

def remove_stop_words():
    stop_words = stopwords.words('russian')
    stop_words.extend(['с', 'на', 'еще', 'ещё'])
    articles_id_lst = [str(article.id) for article in Article.objects]
    for article_id in articles_id_lst:
        an_article_path=ARTICLES_PATH+"/article_"+article_id+".txt"
        article=read_file(an_article_path)
        wrd_lst=[sent.split(' ') for sent in article.split('\n')]
        filtered_words=[]
        for sent in wrd_lst:
            wrds=[]
            for word in sent:
                if word not in stop_words:
                    wrds.append(word.replace('...', '').replace('..', ''))
            filtered_words.append(wrds)
        article='\n'.join([' '.join(sent_words) for sent_words in filtered_words])
        rewrite_file(an_article_path, article)
    print("========================================Stop_words_removed========================================")

def get_environment_words(ent_inds, sents_with_ent):
    ent_dict=dict()
    segmenter = Segmenter()
    emb = NewsEmbedding()
    syntax_parser = NewsSyntaxParser(emb)
    for ind in ent_inds:
        ent_dict[ind]=[]
    for lst in sents_with_ent:
        text, index = get_sentence_and_index(lst)
        doc = Doc(text)
        doc.segment(segmenter)
        doc.parse_syntax(syntax_parser)
        a_token=get_token_with_text(doc, index) 
        print(a_token, index) 
        env_words=[]
        get_lower_words(doc, env_words, a_token)
        get_upper_words(doc, env_words, a_token)
        append_unique_words_to_dict(ent_dict, index, env_words)
    return ent_dict

def replace_index_with_entity(words_lst):
    new_word_lst=[]
    for word in words_lst:
        try:
            not_index_word = Person.objects().get(index=word).name
            new_word_lst.append(not_index_word)
        except:
            try:
                not_index_word = Landmark.objects().get(index=word).name
                new_word_lst.append(not_index_word)
            except:
                new_word_lst.append(word)
    return new_word_lst

def update_env_in_entities_collection(person_env_dict, landmark_env_dict):
    for key in person_env_dict:
        person_env_lst=replace_index_with_entity(person_env_dict[key])
        new_env_lst = list(Person.objects.get(index=key).environment_words)
        for env_word in person_env_lst:
            if env_word not in new_env_lst:
                new_env_lst.append(env_word)
        Person.objects(index=key).update(environment_words=new_env_lst)
    for key in landmark_env_dict:
        landmark_env_lst = replace_index_with_entity(landmark_env_dict[key])
        new_env_lst = list(Landmark.objects.get(index=key).environment_words)
        for env_word in landmark_env_lst:
            if env_word not in new_env_lst:
                new_env_lst.append(env_word)
        Landmark.objects(index=key).update(environment_words=new_env_lst)

def update_syn_in_entities_collection(person_syn_dic, landmark_syn_dic):
    for key in person_syn_dic:
        person_syn_lst=replace_index_with_entity(person_syn_dic[key])
        Person.objects(index=key).update(synonyms=person_syn_lst)
    for key in landmark_syn_dic:
        landmark_syn_lst = replace_index_with_entity(landmark_syn_dic[key])
        Landmark.objects(index=key).update(synonyms=landmark_syn_lst)

def replace_dst_entities_in_articles(articles_id_lst, morph):
    max_number = len(articles_id_lst)-1
    article_number=0
    person_fio_lst=[person.name for person in Person.objects]
    landmark_name_lst=[landmark.name for landmark in Landmark.objects]
    normalized_person_fio_lst=get_normalized_persons_for_replace(person_fio_lst, morph)
    normalized_landmark_name_lst=get_normalized_landmarks_for_replace(landmark_name_lst, morph)
    pers_inds=get_person_indexes()
    lndmrks_inds=get_landmark_indexes()
    sents_with_person=[]
    sents_with_landmark=[]
    for article_id in articles_id_lst:
        an_article_path=ARTICLES_PATH+"/article_"+article_id+".txt"
        print(str(article_number)+" of "+str(max_number))
        print("========================================Replacement_in_the_article:"+an_article_path+"========================================")
        article=read_file(an_article_path)
        for i, normalized_person_fio in enumerate(normalized_person_fio_lst):
            article = replace_person(article_id, article, normalized_person_fio, pers_inds[i], sents_with_person)[0]
            #print("("+str(article_number)+" of "+str(max_number)+") "+normalized_person_fio+" replaced with an index: "+pers_inds[i])
        for j, normal_landmark_name in enumerate(normalized_landmark_name_lst):
            article = replace_landmark(article_id, article, normal_landmark_name, lndmrks_inds[j], sents_with_landmark)[0]
            #print("("+str(article_number)+" of "+str(max_number)+") "+normal_landmark_name+" replaced with an index: "+lndmrks_inds[j])
        rewrite_file(an_article_path, article)
        article_number+=1
    return sents_with_person, sents_with_landmark

def get_environment_ent_words(sents_with_person, sents_with_landmark):
    pers_inds=get_person_indexes()
    lndmrks_inds=get_landmark_indexes()
    person_env_dict=get_environment_words(pers_inds, sents_with_person)
    print("========================================Person_environment_received========================================")
    landmark_env_dict = get_environment_words(lndmrks_inds, sents_with_landmark)
    print("========================================Landmark_environment_received========================================")
    return person_env_dict, landmark_env_dict

def generate_word2vec_model(vector_size, min_count):
    get_new_model(vector_size, min_count)
    print("========================================New_word2vec_model_generated========================================")

def update_environment_words_in_database(person_env_dict, landmark_env_dict):
    update_env_in_entities_collection(person_env_dict, landmark_env_dict)
    print("========================================Environment_updated========================================")

def update_synonyms_in_database():
    pers_inds=get_person_indexes()
    lndmrks_inds=get_landmark_indexes()
    person_syn_dic, landmark_syn_dic = find_synonyms_for_words(' '.join(pers_inds), ' '.join(lndmrks_inds), 10)
    print("========================================Synonyms_received========================================")
    update_syn_in_entities_collection(person_syn_dic, landmark_syn_dic)
    print("========================================Entities_updated========================================")

def write_sents_with_ent_to_db(sents_with_person, sents_with_landmark):
    new_sents_id_lst = fill_Sentences_collection(sents_with_person, sents_with_landmark)
    print("========================================Sentences_collection_filled========================================")
    return new_sents_id_lst

if __name__ == '__main__':
    with connection():
        morph = pymorphy2.MorphAnalyzer()
        generate_article_files(morph)
        replace_dst_entities_in_articles(morph)
        #print(get_synonyms_for_word("PersonIndexAA", 50))
