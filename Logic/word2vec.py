import sys
sys.path.insert(0, '.') #Пародосально, но без этого constants больше не нужны
sys.path.insert(1, '..')

try:
    from pyspark.ml.feature import Word2Vec, Word2VecModel
    from pyspark.sql import SparkSession
    from pyspark.sql.functions import format_number as fmt
except:
    pass
from constants import WORD2VEC_MODEL_PATH, ARTICLES_PATH, LOGIC_PATH
import os
from sys import argv
import re

#word - словл, для которого ищутся синонимы
#количество получаемых синониимов
#На выходе: [[слово, ...], [сходство, ...]]
def get_synonyms_for_word(word, count):
    synonyms_lst=[]
    word2vec_model_path = WORD2VEC_MODEL_PATH
    spark = SparkSession.builder.appName("SimpleApplication").getOrCreate()
    loaded_model = Word2VecModel.read().load(word2vec_model_path)
    #loaded_model.getVectors().show(50) #раскомментировав можно посмотреть на вектора слов
    try:
        df=loaded_model.findSynonyms(word, count).select("word", fmt("similarity", 5).alias("similarity"))#.show() #Раскомментировав можно посмотреть вывод на консоль
        words=[''.join(*row) for row in df.select("word").collect()]
        #similarity=[''.join(*row) for row in df.select("similarity").collect()] #Оценка схожести
        synonyms_lst=words
    except Exception as e:
        #print(e)
        print("Синонимы отсутствуют: "+word)
        synonyms_lst=[]
    spark.stop()
    return synonyms_lst

def write_dicts_to_exchanger(entity_dict):
    with open("Data/exchanger.txt", "w") as exchanger:
        first_key=True
        for dict_key in entity_dict:
            if first_key:
                first_key=False
            else:
                exchanger.write("\n##########\n")
            first_line=True
            for key in entity_dict[dict_key]:
                if first_line:
                    first_line=False
                else:
                    exchanger.write("\n")
                exchanger.write(str(key)+"**********")
                first_word=True
                for value in entity_dict[dict_key][key]:
                    if first_word:
                        first_word=False
                    else:
                        exchanger.write("@@@@@@@@@@")
                    exchanger.write(value)

def read_dicts_from_exchanger():
    with open("Data/exchanger.txt", "r") as exchanger:
        text=exchanger.read()
        dicts=text.split("\n##########\n")
        dic_keys=["person", "landmark"]
        ent_dicts=dict()
        for i, dic in enumerate(dicts):
            lines=dic.split("\n")
            an_ent_dict=dict()
            for line in lines:
                line_part=line.split("**********")
                key=line_part[0]
                words_lst=line_part[1].split("@@@@@@@@@@")
                if words_lst[0]=='':
                    words_lst=[]
                an_ent_dict[key]=words_lst
            ent_dicts[dic_keys[i]]=an_ent_dict
    return ent_dicts
                


def get_syn(loaded_model, entities, count):
    entity_dict={"person": dict(), "landmark": dict()}
    #loaded_model.getVectors().show(50) #раскомментировав можно посмотреть на вектора слов
    for ent in entities:
        ent_index, ent_type = ent[0], ent[1]
        try:
            df=loaded_model.findSynonyms(ent_index, 100).select("word", fmt("similarity", 5).alias("similarity"))#.show() #Раскомментировав можно посмотреть вывод на консоль
            words=[''.join(*row) for row in df.select("word").collect()]
            syn_lst=[]
            for word in words:
                if re.findall(r"[а-яё\-a-z]+", word) and not re.findall(r"\d+\-\d+", word):
                    syn_lst.append(word)
                    if len(syn_lst)==count:
                        break          
            #similarity=[''.join(*row) for row in df.select("similarity").collect()]
            synonyms_lst=syn_lst
        except Exception as e:
            #print(e)
            #print("Синонимы отсутствуют: "+ent_index)
            synonyms_lst=[]
        entity_dict[ent_type][ent_index]=synonyms_lst
    return entity_dict#entity_dict["person"], entity_dict["landmark"]

def get_synonyms_for_words(persons, landmarks, count):
    word2vec_model_path = WORD2VEC_MODEL_PATH
    spark = SparkSession.builder.appName("SimpleApplication").getOrCreate()
    loaded_model = Word2VecModel.read().load(word2vec_model_path)
    #person_dict, landmark_dict=get_syn(loaded_model, [[pers, "person"] for pers in persons]+[[land, "landmark"] for land in landmarks], count)
    entity_dict=get_syn(loaded_model, [[pers, "person"] for pers in persons]+[[land, "landmark"] for land in landmarks], count)
    spark.stop()
    write_dicts_to_exchanger(entity_dict)
    #return person_dict, landmark_dict

#vector_size - рамер вектора, который будет создаватся для каждого слова
#min_count - количество упоменаний слов в датафрейме, ниже которого слова будут считатся опечатками/незначительными и отбрасыватся
def generate_new_model(vector_size, min_count):
    word2vec_model_path = WORD2VEC_MODEL_PATH
    os.system("rm -R "+WORD2VEC_MODEL_PATH)                  
    os.mkdir(word2vec_model_path)
    spark = SparkSession.builder.appName("SimpleApplication").getOrCreate()
    input_files = spark.sparkContext.wholeTextFiles(ARTICLES_PATH+'/*.txt')
    prepared_data = input_files.flatMap(lambda x:([([word for word in sent.split(' ')],) for sent in x[1].split('\n')])) #x[0] путь, x[1] содержимое файла, тебуемая структкра после flatMap [([слово предложения, ...], ...), ([слово предложения, ...], ...), ...]
    df=prepared_data.toDF().selectExpr('_1 as sentences')
    word2vec = Word2Vec(vectorSize=vector_size, minCount=min_count, seed=42, inputCol="sentences", outputCol="model")
    model = word2vec.fit(df)  
    model.write().overwrite().save(word2vec_model_path)
    #df.show(20, truncate=True) #Посмотреть на консоли 20 записей
    model.findSynonyms("рассказать", 10).select("word", fmt("similarity", 5).alias("similarity"))
    spark.stop()                                                          

def get_new_model(vector_size, min_count):
    os.system("spark-submit "+LOGIC_PATH+"/word2vec.py "+"model "+str(vector_size)+' '+str(min_count))  #Обходим невозможность обучить и сохранить модель, а после загрузить и получить синонимы за один прогон скрипта для данной версии pyspark

def find_synonyms_for_words(persons, landmarks, count):
    os.system("spark-submit "+LOGIC_PATH+"/word2vec.py "+"synonyms "+str(count)+' '+str(persons)+' '+str(landmarks))
    entity_dict=read_dicts_from_exchanger()
    return entity_dict["person"], entity_dict["landmark"]

if __name__ == '__main__':
    func_type = argv[1]
    if func_type=="synonyms":
        count = int(argv[2])
        persons=[]
        landmarks=[]
        for item in argv:
            itm = str(item)
            if itm.startswith("PersonIndex"):
                persons.append(itm)
            elif itm.startswith("LandmarkIndex"):
                landmarks.append(itm)
        get_synonyms_for_words(persons, landmarks, count)
    elif func_type=="model":
        vector_size, min_count = argv[2:]
        generate_new_model(int(vector_size), int(min_count))