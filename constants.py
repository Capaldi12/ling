import os

PROJECT_PATH = os.getcwd()  #"/home/vagrant/KW/ling"
EXCHANGER_PATH = PROJECT_PATH + '/Data/exchanger.txt'
ARTICLES_PATH = PROJECT_PATH + "/Data/Articles"
LANDMARKS_URL = "https://avolgograd.com/sights?obl=vgg"
PERSONS_URL = "https://global-volgograd.ru/person?offset="
WORD2VEC_MODEL_PATH = PROJECT_PATH + "/Data/Word2VecModel"
CHROMDRIVER_PATH = PROJECT_PATH + "/Drivers/chromedriver"
LOGIC_PATH = PROJECT_PATH +"/Logic"

TWEETS_PATH = os.path.join(PROJECT_PATH, 'Data/Tweets')
CLASSIFIER_PATH = os.path.join(PROJECT_PATH, 'Data/Tonality/model.pickle')
