"""Парсинг сайта"""
from bs4 import BeautifulSoup

import requests
from requests.exceptions import RequestException

from mongo import connection, Article
from functions import parse_date

from tqdm import trange
import time


from typing import List


v1 = 'https://v1.ru'


# Исключения для более гибкой обработки ошибок
class ParserException(Exception):
    pass

class LinksLoadException(ParserException):
    pass

class ArticleLoadException(ParserException):
    pass


def get_soup(url: str) -> BeautifulSoup:
    """
    Скачивает страницу по заданному адресу и создаёт для неё "суп"

    :param url: Адрес страницы
    :return: "Суп" страницы
    """

    r = requests.get(url)
    soup = BeautifulSoup(r.content, features="lxml")

    return soup


def get_links(page: int = None):
    """
    Получает ссылки на новости с заданной страницы списка новостей сайта

    :param page: Номер страницы
    :return: Список ссылок
    """

    try:

        if page:
            assert page >= 0, 'Page number must be positive'

            # Если страница указана, передаём её как аргумент
            soup = get_soup(v1 + f'/text/?page={page}')
        else:

            # Если нет, просто скачиваем первую страницу
            soup = get_soup(v1 + '/text/')

    except RequestException as e:
        raise LinksLoadException('Cannot load links') from e

    # Ищем все теги article. В них первый дочерний тег
    # будет содержать ссылку на статью в аттрибуте href
    links = [next(article.children).attrs['href']
             for article in soup.find_all('article')][::-1]

    # Тут встречаются иногда ссылки на longread - длинные статьи
    # Они работают по другим правилам, поэтому их пока не парсим
    # Ссылки на них даются полные - с адресом сайта и протоколом
    # (в отличие от обычных, которые начинаются с /text)
    # поэтому их легко отфильтровать
    return [v1 + link for link in links if link[:5] == '/text']


def get_article(url: str, article: Article = None) -> Article:
    """
    Парсит статью по заданному адресу и собирает документ Article

    :param url: Адрес статьи
    :param article: Объект статьи для обновления
    :return: Документ Article
    """
    try:

        soup = get_soup(url)

    except RequestException as e:
        raise ArticleLoadException('Cannot load article') from e

    # Если объект для обновления не предоставлен - собрать новый
    # Для готового объекта просто обновляем количество просмотров и комментариев
    if not article:
        # На v1 все нужные нам теги имеют аттрибут itemprop
        # Это позволяет быстро найти и выбрать эти теги
        propped = soup.find_all(lambda tag: "itemprop" in tag.attrs)


        # <> datePublished
        # Тут дата публикации вида 2021-04-29T13:00:00+03:00
        # Находится два тега, meta из шапки и какой-то странный div
        # Поэтому сверяю название тега
        date_prop = next(item for item in propped if item.name == 'meta'
                         and item.attrs['itemprop'] == 'datePublished')

        date_published = parse_date(date_prop.attrs['content'])


        # <> headline
        # Заголовок статьи
        headline_prop = next(tag for tag in propped
                             if tag.attrs['itemprop'] == 'headline')

        headline = headline_prop.text


        # <> articleBody
        # Текст статьи. Вроде только сам текст, подписи к картинкам не захватывает
        body_prop = next(tag for tag in propped
                         if tag.attrs['itemprop'] == 'articleBody')

        # Неа, подписи тоже ловим. От них нужно избавиться сначала
        # Судя по всему, весь нужный нам текст сидит конкретно в тегах 'p'
        # Но и весь ненужный тоже. Но у ненужного есть аттрибуты
        # А у нужного их нет. Вроде бы всегда так
        # Поэтому оставляем только те теги, у которых аттрибутов нет
        content = [p.text for p in body_prop.find_all('p')if len(p.attrs) == 0]

        # Попытался искать и доставать ссылку на видео, но судя по всему,
        # оно подгружается как-то динамически, в сыром html его вообще нет.
        # Поэтому найти его не представляется возможным,
        # с моим пониманием всего этого, по крайней мере

        # Компонуем объект статьи
        article = Article(link=url, title=headline,
                          content=content, date=date_published)


    # Просмотры уже не имеют itemprop, поэтому ищем их через значение аттрибута title
    try:
        views_prop = soup.find_all('div', {'title': 'просмотры'})[0]

        # Превращаем в число, предварительно избавившись от всяких там неразрывных пробелов
        article.views = int(views_prop.text.encode('ascii', 'ignore'))

    except IndexError:
        article.views = 0

    # С комментами то же самое
    # Но ловим ошибку при конверсии в число
    # Так как когда комментов нет, они число не показывают вообще
    # Так же ловим ошибку, если комментов вообще на предусмотрено для статьи
    try:
        comments_prop = soup.find_all('a', {'title': 'комментарии'})[0]
        article.comments = int(comments_prop.text.encode('ascii', 'ignore'))

    except (ValueError, IndexError):
        article.comments = 0

    # Упаковываем все собранные данные в документ и возвращаем
    return article


def parse_from_page(page: int = None) -> List[str]:
    """
    Парсит новости с заданной (или первой) страницы сайта,
    сохраняя или обновляя записи в БД

    :param page: Номер страницы
    """

    # Открываем страницу, берем ссылки
    links = get_links(page)

    successful = []

    # Парсим все статьи по ссылкам и пихаем в бд
    for i in trange(len(links), desc=f'Parsing page {page or 1:2}'):
        link = links[i]

        a = Article.objects(link=link).first()  # Ищем статью в бд

        try:

            # Парсим страницу статьи
            # Вторым аргументом передается статья, если она уже есть в БД
            article = get_article(link, a)
            article.save()

            successful.append(str(article.id))

        except ArticleLoadException:
            pass

    return successful


def parse_first_pages(count: int = 10):
    for page in range(count, 0, -1):

        parse_from_page(page)
        time.sleep(3)


if __name__ == '__main__':
    from pprint import pprint

    with connection():
        parse_first_pages(20)
